# Raspberry Pi 4-channel relay test

## Purpose

This simple sketch is designed to test a 4-channel relay by waiting for a user
input integer between 1 and 4.  A selected relay module will be turned off if it
is on, and on if it is off.

To end the test, use a keyboard interrupt: `Ctrl + c`

## Downloading

    cd ~/
    git clone https://bitbucket.org/mdill/rPi_relay_selectable.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/rpi_relay_selectable/src/6b87ad66b802f1ed5bad38e58565dde92da3ca98/LICENSE.txt?at=master) file for
details.

