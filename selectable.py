"""
20151120 - Created by Michael Dill (c)

This sketch is designed test a 4-channel relay by cycling through each module
when the user prompts the sketch with a number between 1 and 4.

Once a keyboard interrupt is noticed each GPIO will turn off at the end.

"""

import RPi.GPIO as GPIO

# Set the GPIO pins for each individual relay
           #  R1  R2  R3  R4
relay_list = [11, 13, 15, 16]

# Set initial states for all relays to off
relay_state = [1, 1, 1, 1]

GPIO.setmode(GPIO.BOARD)

# Set each relay pin as an output
for x in relay_list:
    GPIO.setup(x, GPIO.OUT)

try:
    while True:

        # Turn all relays on or off according to their current status
        for i in range (0,4):
            GPIO.output(relay_list[i], relay_state[i])

        # Get relay selection from the user
        select = int(input('\n1-4: '))
        if (select != 1 and select != 2 and select != 3 and select != 4):
            print('Your choice is bad and you should feel bad!')
        else:
            if (relay_state[select] == 0):
                relay_state[select] = 1
            else:
                relay_state[select] = 0

except KeyboardInterrupt:
    print("\nA keyboard interrupt has been detected!")

except:
    print("\nAn error or exception has occurred!")

finally:
    GPIO.cleanup()
